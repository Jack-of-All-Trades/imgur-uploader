# We name the `base` stage so we can refence it in multiple later
# stages but only need to update it in one place if we change it
FROM node:10.16.3-jessie AS Base

RUN mkdir /app /app/download /app/logs
WORKDIR /app

COPY ./ /app

RUN npm install; npm run build

#The resulting image will run the application using a
# production webserver and configuration
FROM keymetrics/pm2:10-alpine as Prod
WORKDIR /app

ENV NODE_ENV=production
COPY --from=Base ./app/ .

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

EXPOSE 3000

CMD ["pm2-docker", "start", "--json", "./.build/main.js"]

# The `Dev` stage is the default stage if the Dockerfile is run without 
# a target stage set. `Dev` runs the application using the development 
#web server, and enables developer tools like the debugger and interactive expcetions
FROM node:10.16.3-alpine as Dev
WORKDIR /app
ENV NODE_ENV=development
COPY --from=Base ./app/ .
CMD npm run dev