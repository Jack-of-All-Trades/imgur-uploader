var express = require("express");
var server = express();
var uploadRouter = require("./routes/upload");
var imagesRouter = require("./routes/images");
var morgan = require('morgan');
var winston = require('./config/winston');

//require('./model/procmodel');

const PORT = 3000;
server.use(
  express.json({
    inflate: true,
    limit: "100kb",
    reviver: null,
    strict: true,
    type: "application/json",
    verify: undefined
  })
);
server.use(morgan('combined', { stream: winston.stream }));
server.use("/v1/images/upload", uploadRouter);
server.use("/v1/images", imagesRouter);
// server.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
server.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  //winston logging
  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  // render the error page
  res.status(err.status || 500);
  res.json(err.message);
});
server.listen(PORT, () => {
  console.log("Listening at " + PORT);
});
module.exports = server;
