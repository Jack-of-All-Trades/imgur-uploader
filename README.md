## Imgur Uploader

A simple nodejs rest-api based application used to upload, retreieve, and display uploading status for imgur. 

As per the requirement, images are suppose to be saved before uploading to imgur. The way i've coded the program is to download all given images on to a local file system and then start uploading. If the image is successfully uploaded, it'll be deleted from the file system.

**please get the client id and secret before running the steps below or you may use the ones mentioned below**

**Production**


Image: docker build . -t imgur-uploader-prod --target=Prod

Run: docker run -p 3000:3000 -e clientID=fefa92366568e31 -e secret=db80df92e428f91e70645fb0eaeeabfd475c5cc7 -d imgur-uploader-prod


If you don't wish to build the image, you can pull it from

docker pull abhishiekgowtham/imgur-uploader-prod


**Development**


Image build: 


Run: docker run -p 3000:3000 -e clientID=fefa92366568e31 -e secret=db80df92e428f91e70645fb0eaeeabfd475c5cc7 imgur-uploader-dev


If you don't wish to build the image, you can pull it from

docker pull abhishiekgowtham/imgur-uploader-dev



Application will start running at http://localhost:3000
