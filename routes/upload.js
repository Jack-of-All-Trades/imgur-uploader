var express = require("express");
const uuidv4 = require("uuid/v4");
var imgur = require("imgur");
var request = require("request");
var requests = require("request-promise");
var fs = require("fs");
var async = require("async");
var logger = require('../config/winston'); 
var router = express.Router();

require("../model/procmodel");

imgur.setClientId(`${process.env.clientID}`);

function pad(number, length, padChar) {
  if (typeof length === "undefined") length = 2;
  if (typeof padChar === "undefined") padChar = "0";
  var str = "" + number;
  while (str.length < length) {
    str = padChar + str;
  }
  return str;
}

 // get current date and time
function getOffsetFromUTC() {
  var offset = new Date().getTimezoneOffset();
  return (
    (offset < 0 ? "+" : "-") +
    pad(Math.abs(offset / 60), 2) +
    ":" +
    pad(Math.abs(offset % 60), 2)
  );
}

// callback func for uploading image
function callbackUpload(status, jobid, url, req) {
  var jobObj = g_mapJob.get(jobid);
  var json = null;
  if (req) {
    json = req.data;
  }

  for (var i in jobObj.urls) {
    if (jobObj.urls[i].url == url) {
      jobObj.urls[i].status = status;
      if (status == "Completed" && json) jobObj.urls[i].url = json.link;
      break;
    }
  }
  jobObj.count++;
  if (jobObj.count == jobObj.urls.length) {
    var date = new Date();
    jobObj.finished =
      date.getFullYear() +
      "-" +
      (date.getMonth() + 1) +
      "-" +
      date.getDate() +
      "T" +
      date.getHours() +
      ":" +
      date.getMinutes() +
      ":" +
      date.getSeconds() +
      getOffsetFromUTC();
  }
  g_mapJob.set(jobid, jobObj);
}

// upload image to imgur
function uploadtoImgur(jobid, url, func) {
  var file_name = url.split("/").pop();
  var dfile = "download/" + file_name;
  imgur
    .uploadFile(dfile)
    .then(function(json) {
      fs.unlinkSync(dfile);
      func("Completed", jobid, url, json);
    })
    .catch(function(err) {
      logger.error(err.message);
      //fs.unlinkSync(dfile);
      func("Failed", jobid, url, null);
    });

  return;
}

 // download image on to the file system
const downloadImage = function(jobid, url, func) {
  var res = "";
  var file_name = url.split("/").pop();
  var res = request(url);
  res.on("response", function(response) {
    if (response.statusCode === 200) {
      response.pipe(fs.createWriteStream("download/" + file_name));
    } else {
      func("Failed", jobid, url, null);
    }
  });
  res.on("end", () => {
    uploadtoImgur(jobid, url, func);
  });
  res.on("error", e => {
    logger.error(e.message);
    func("Failed", jobid, url, null);
  });
};

/* GET users listing. */
router.post("/", function(req, res, next) {
  var jobObj = new Object();
  var uuid = uuidv4();
  var date = new Date();
  var days = ["M", "T"];
  var createtime =
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getDate() +
    "T" +
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds() +
    getOffsetFromUTC();
  jobObj.jobID = uuid;
  jobObj.created = createtime;
  jobObj.count = 0;
  jobObj.urls = new Array();
  g_mapJob.set(uuid, jobObj);
  const limitation = [
    function() {
      for (var url in req.body.urls) {
        var oldurl = req.body.urls[url];
        var obj = { url: oldurl, status: "pending" };
        jobObj.urls.push(obj);
        downloadImage(jobObj.jobID, oldurl, callbackUpload);
      }
    }
  ];
  async.parallelLimit(limitation, 10, function(err) { logger.error(err) });
  g_mapJob.set(uuid, jobObj);
  var obj = new Object();
  obj.jobid = jobObj.jobID;
  res.send(JSON.stringify(obj));
});

// get status based on jobid
router.get("/:jobid", function(req, res, next) {
  var obj = new Array();

  var key = req.params.jobid;
  var value = g_mapJob.get(key);
  {
    var objsub = new Object();
    objsub.jobid = key;
    objsub.created = value.created;
    objsub.finished = null;
    objsub.pending = new Array();
    objsub.failed = new Array();
    objsub.uploaded = new Array();

    for (var i in value.urls) {
      if (value.urls[i].status == "Completed") {
        objsub.uploaded.push(value.urls[i].url);
      } else if (value.urls[i].status == "Failed") {
        objsub.failed.push(value.urls[i].url);
      } else {
        objsub.pending.push(value.urls[i].url);
      }
    }
    if (value.urls.length == objsub.uploaded.length)
      objsub.status = "Completed";
    else if (objsub.uploaded.length == 0 && objsub.failed.length == 0)
      objsub.status = "pending";
    else if (value.created) objsub.status = "Completed";
    else objsub.status = "in-progress";
    objsub.finished = objsub.uploaded.length;
    obj.push(objsub);
  }
  res.send(JSON.stringify(obj));
});

module.exports = router;
