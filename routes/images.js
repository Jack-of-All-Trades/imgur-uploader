var express = require('express');
var router = express.Router();

require('../model/procmodel'); 

/* GET users listing. */
router.get('/', function(req, res, next) {
    var obj = new Object();
    obj.uploaded = new Array();
    for (var [key, value] of g_mapJob) {
        for(var i in value.urls) {
            if(value.urls[i].status == "Completed") {
                obj.uploaded.push(value.urls[i].url);
            }
        }
    }
    res.send(JSON.stringify(obj));
});

module.exports = router;
